<?php namespace Sschlein\OneClickSubscription\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 *
 * @package Sschlein\OneClickSubscription\Helper
 */
class Config
{
    /**
     * coniguration secret salt
     */
    const CONFIG_PATH_SECRET = 'oneclicksubscription/general/secret';

    /**
     * coniguration default redirect
     */
    const CONFIG_PATH_DEFAULT_REDIRECT = 'oneclicksubscription/general/redirect';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;


    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->scopeConfig->getValue(Config::CONFIG_PATH_SECRET, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getDefaultRedirect()
    {
        return $this->scopeConfig->getValue(Config::CONFIG_PATH_DEFAULT_REDIRECT, ScopeInterface::SCOPE_STORE);
    }
}
