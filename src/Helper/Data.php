<?php namespace Sschlein\OneClickSubscription\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /** @var Config  */
    protected $config;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param Config $config
     */
    public function __construct(
        Context $context,
        Config $config
    )
    {
        $this->config = $config;

        parent::__construct($context);
    }

    /**
     * @param string $email
     * @return string
     */
    public function generateHash($email)
    {
        return hash('sha256', $email . $this->config->getSecret());
    }
}
