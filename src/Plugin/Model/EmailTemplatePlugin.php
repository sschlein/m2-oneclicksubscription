<?php

namespace Sschlein\OneClickSubscription\Plugin\Model;

use Magento\Email\Model\Template as Subject;
use Magento\Store\Model\StoreManagerInterface;
use Sschlein\OneClickSubscription\Helper\Data;

class EmailTemplatePlugin
{
    /** @var StoreManagerInterface  */
    protected $storeManager;

    /** @var Data */
    private $helper;

    public function __construct(
        StoreManagerInterface $storeManager,
        Data $helper
    )
    {
        $this->storeManager = $storeManager;
        $this->helper = $helper;
    }

    public function beforeGetProcessedTemplate(Subject $subject, array $variables = [])
    {
        $email = $this->getEmail($variables);

        if(!$email) {
            return [$variables];
        }

        if($this->isSubscribed($email)) {
            return [$variables];
        }

        $variables['ocs_user_not_subscribed'] = true;
        $variables['ocs_subscription_link'] = $this->storeManager->getStore()->getBaseUrl() . 'oneclicksubscription?email=' . urlencode($email) . '&hash=' . $this->helper->generateHash($email);

        return [$variables];
    }

    protected function getEmail(array $variables)
    {
        $email = '';

        if (isset($variables['customer'])) { /** @var @TODO Typ prüfen $email */
            $email = $variables['customer']->getEmail();
        }

        return $email;
    }

    protected function isSubscribed($email)
    {
        return false;
    }
}