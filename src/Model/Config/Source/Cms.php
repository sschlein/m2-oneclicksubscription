<?php namespace Sschlein\OneClickSubscription\Model\Config\Source;

use Magento\Cms\Model\Page;
use Magento\Framework\Option\ArrayInterface;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;

class Cms implements ArrayInterface
{
    /** @var CollectionFactory  */
    protected $pageCollectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->pageCollectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $res = [];
        $collection = $this->pageCollectionFactory->create()->addFieldToFilter('is_active' , Page::STATUS_ENABLED);

        foreach($collection as $page) {
            $data['value'] = $page->getData('identifier');
            $data['label'] = $page->getData('title');
            $res[] = $data;
        }

        return $res;
    }
}