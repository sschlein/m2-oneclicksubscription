<?php namespace Sschlein\OneClickSubscription\Controller\Index;

use Exception;
use Zend_Validate;
use Zend_Validate_NotEmpty;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Newsletter\Model\Subscriber;
use Magento\Framework\App\Action\Context;
use Sschlein\OneClickSubscription\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Sschlein\OneClickSubscription\Helper\Config as ModuleConfig;

class Index extends Action {

    /** @var Session  */
    protected $customerSession;

    /** @var Subscriber  */
    protected $subscriber;

    /** @var SubscriberFactory  */
    protected $subscriberFactory;

    /** @var StoreManagerInterface */
    protected $storeManager;

    /** @var JsonFactory  */
    protected $resultJsonFactory;

    /** @var RedirectFactory  */
    protected $resultRedirectFactory;

    /** @var ScopeConfigInterface  */
    protected $config;

    /** @var ModuleConfig  */
    protected $moduleConfig;

    /** @var Data  */
    protected $helper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Session $customerSession
     * @param Subscriber $subscriber
     * @param SubscriberFactory $subscriberFactory
     * @param StoreManagerInterface $storeManager
     * @param JsonFactory $resultJsonFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param ScopeConfigInterface $config
     * @param Data $helper
     * @param ManagerInterface $messageManager
     * @param ModuleConfig $moduleConfig
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Subscriber $subscriber,
        SubscriberFactory $subscriberFactory,
        StoreManagerInterface $storeManager,
        JsonFactory $resultJsonFactory,
        RedirectFactory $resultRedirectFactory,
        ScopeConfigInterface $config,
        Data $helper,
        ManagerInterface $messageManager,
        ModuleConfig $moduleConfig
    ) {
        $this->config = $config;
        $this->helper = $helper;
        $this->moduleConfig = $moduleConfig;
        $this->subscriber = $subscriber;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->customerSession = $customerSession;
        $this->subscriberFactory = $subscriberFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;

        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = [
            'hash' => (string) $this->getRequest()->getParam('hash'),
            'email' => (string) $this->getRequest()->getParam('email'),
            'store_id' => (int) $this->getRequest()->getParam('store_id'),
            'first_name' => (string) $this->getRequest()->getParam('first_name'),
            'last_name' => (string) $this->getRequest()->getParam('last_name'),
            'referrer' => (string) $this->getRequest()->getParam('referrer'),
            'redirect' => (string) $this->getRequest()->getParam('redirect')
        ];

        if(empty($data['store_id'])) {
            $data['store_id'] = $this->storeManager->getStore()->getId();
        }

        $errors = $this->_validateInputParams($data);

        if (!empty($errors)) {
            return $this->resultJsonFactory->create()
                ->setHttpResponseCode(400)
                ->setData(['error' => $errors]);
        }

        try {

            $this->storeManager->setCurrentStore($data['store_id']);

            $this->_checkIfSubscriptionIsAllowed();

            $subscriber = $this->subscriber->loadByEmail($data['email']);

            if (!$subscriber->getId()) {
                $status = $this->subscriberFactory->create()->subscribe($data['email']);
            }

            if(!$this->getRequest()->isXmlHttpRequest()) {

                if(isset($status)) {
                    if ($status == \Magento\Newsletter\Model\Subscriber::STATUS_NOT_ACTIVE) {
                        $this->messageManager->addSuccessMessage(__('The confirmation request has been sent.'));
                    } else {
                        $this->messageManager->addSuccessMessage(__('Thank you for your subscription.'));
                    }
                }

                if(empty($data['redirect'])) {
                    return $this->resultRedirectFactory->create()->setPath($this->moduleConfig->getDefaultRedirect());
                } else {
                    if(strpos($data['redirect'], 'http') === 0) {
                        return $this->resultRedirectFactory->create()->setUrl($data['redirect']);
                    }
                    return $this->resultRedirectFactory->create()->setPath($data['redirect']);
                }
            }

            return $this->resultJsonFactory->create()
                ->setData(['message' => 'OK']);

        } catch (Exception $e) {
            return $this->resultJsonFactory->create()
                ->setHttpResponseCode(500)
                ->setData(['error' => [$e->getMessage()]]);
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected function _validateInputParams(array $data)
    {
        $errors = [];

        if (!Zend_Validate::is($data['hash'], 'NotEmpty')) {
            $errors[] = 'hash is required';
            return $errors;
        }

        if (!Zend_Validate::is($data['email'], 'NotEmpty')) {
            $errors[] = 'email is required';
        } else {
            // Only validate address if email given.
            if (!Zend_Validate::is($data['email'], 'EmailAddress')) {
                $errors[] = 'email is not a valid email address';
            }
        }

        if(!$this->moduleConfig->getSecret()) {
            $errors[] = 'hash is not set in store configuration';
            return $errors;
        }

        if (!$this->_isValidHash($data['hash'], $data['email'])) {
            $errors[] = 'hash is invalid';
            return $errors;
        }

        if (!Zend_Validate::is($data['store_id'], 'NotEmpty', ['type' => Zend_Validate_NotEmpty::INTEGER])) {
            if (!$this->_isValidStore($data['store_id'])) {
                $errors[] = 'store_id is invalid';
            }
        }

        if (!Zend_Validate::is($data['first_name'], 'StringLength', ['max' => 100])) {
            $errors[] = 'first_name is too long (max of 100 chars)';
        }

        if (!Zend_Validate::is($data['last_name'], 'StringLength', ['max' => 100])) {
            $errors[] = 'last_name is too long (max of 100 chars)';
        }

        if (!Zend_Validate::is($data['referrer'], 'StringLength', ['max' => 50])) {
            $errors[] = 'referrer is too long (max of 50 chars)';
        }

        return $errors;
    }

    /**
     * @param string $hash
     * @param string $email
     * @return bool
     */
    protected function _isValidHash($hash, $email)
    {
        $expectedHash = $this->helper->generateHash($email);

        if (hash_equals($expectedHash, $hash)) {
            return true;
        }

        return false;
    }

    /**
     * @param int $storeId
     * @return bool
     */
    protected function _isValidStore($storeId)
    {
        $store = $this->storeManager->getStore($storeId);

        if ($store->getId()) {
            return true;
        }

        return false;
    }

    protected function _checkIfSubscriptionIsAllowed()
    {
        if ($this->config->getValue(
                \Magento\Newsletter\Model\Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ) != 1
            && ! $this->customerSession->isLoggedIn()
        ) {
            return $this->resultJsonFactory->create()
                ->setHttpResponseCode(403)
                ->setData(['error' => 'guest subscriptions are not allowed']);
        }
    }
}