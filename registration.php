<?php
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Sschlein_OneClickSubscription',
    __DIR__ . DIRECTORY_SEPARATOR . 'src'
);
