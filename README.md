# Documentation

## Email Template Variables

The module adds variables to all email templates automatically.

* *ocs_user_not_subscribed*: true/false - The email address if the receiver is checked against the subscriber list
* *ocs_subscription_link*: The root link element for all data available. 

## Example 

Most basic example for a link that is only visible for receivers that are not in the newsletter subscriber list yet.

```html
{{if ocs_user_not_subscribed}}
<a href="{{var ocs_subscription_link}}"  target="_blank">Subscribe to our newsletter with 1 click</a>
<br><br>
{{/if}}
```
